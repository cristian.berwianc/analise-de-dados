<b>Descrição:</b><br>
Criar um sistema de análise de dados de venda que irá importar lotes de arquivos e produzir um relatório baseado em informações presentes no mesmo.<br>
Existem 3 tipos de dados dentro dos arquivos e eles podem ser distinguidos pelo seu identificador que estará presente na primeira coluna de cada linha, onde o separador de colunas é o caractere “ç”.

<b>Dados do vendedor</b><br>
Os dados do vendedor possuem o identificador 001 e seguem o seguinte formato:<br>
001çCPFçNameçSalary

<b>Dados do cliente</b><br>
Os dados do cliente possuem o identificador 002 e seguem o seguinte formato:<br>
002çCNPJçNameçBusiness Area

<b>Dados de venda</b><br>
Os dados de venda possuem o identificador 003 e seguem o seguinte formato:<br>
003çSale IDç[Item ID-Item Quantity-Item Price]çSalesman name

<b>Exemplo de conteúdo total do arquivo:</b><br>
001ç1234567891234çPedroç50000<br>
001ç3245678865434çPauloç40000.99<br>
002ç2345675434544345çJose da SilvaçRural<br>
002ç2345675433444345çEduardo PereiraçRural<br>
003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro<br>
003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo<br>

O sistema deverá ler continuamente todos os arquivos dentro do diretório padrão
HOMEPATH/data/in e colocar o arquivo de saída em HOMEPATH/data/out.<br>
No arquivo de saída o sistema deverá possuir os seguintes dados:<br>
• Quantidade de clientes no arquivo de entrada<br>
• Quantidade de vendedores no arquivo de entrada<br>
• ID da venda mais cara<br>
• O pior vendedor<br>

<b>Requisitos técnicos</b><br>
• O sistema deve rodar continuamente e capturar novos arquivos assim que eles sejam inseridos no diretório padrão.<br>
• Você tem total liberdade para escolher qualquer biblioteca externa se achar necessário.
