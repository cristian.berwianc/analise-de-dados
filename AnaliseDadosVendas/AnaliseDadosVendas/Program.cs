﻿using AnaliseDadosVendas.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AnaliseDadosVendas
{
    class Program
    {
        private static string DiretorioImportacaoIn;
        private static string DiretorioImportacaoOut;
        private static string ExtensaoArquivoImportacao;

        static void Main(string[] args)
        {
            DiretorioImportacaoIn = ConfigurationManager.AppSettings["DIRETORIO_IMPORTACAO_IN"].ToString();
            DiretorioImportacaoOut = ConfigurationManager.AppSettings["DIRETORIO_IMPORTACAO_OUT"].ToString();
            ExtensaoArquivoImportacao = ConfigurationManager.AppSettings["EXTENSAO_ARQUIVOS_IMPORTACAO"].ToString();

            Run();
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void Run()
        {
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = DiretorioImportacaoIn;
                watcher.Filter = ExtensaoArquivoImportacao;
                watcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName;
                watcher.Created += OnCreated;
                watcher.EnableRaisingEvents = true;

                Console.ReadKey();
            }
        }

        private static void OnCreated(object source, FileSystemEventArgs e)
        {
            try
            {
                Console.WriteLine("Arquivo: {0}", e.FullPath);

                while (IsFileLocked(e.FullPath))
                {
                    Thread.Sleep(500);
                }

                ConcurrentBag<Vendedor> vendedores = new ConcurrentBag<Vendedor>();
                ConcurrentBag<Cliente> clientes = new ConcurrentBag<Cliente>();
                ConcurrentBag<Venda> vendas = new ConcurrentBag<Venda>();

                Parallel.ForEach(File.ReadLines(e.FullPath, Encoding.Default), line =>
                {
                    var data = line.Split('ç');

                    if (data != null && data.Length > 0)
                    {
                        switch (data[0])
                        {
                            case "001":
                                DadosVendedor(data, vendedores);
                                break;
                            case "002":
                                DadosCliente(data, clientes);
                                break;
                            case "003":
                                DadosVenda(data, vendas);
                                break;
                            default:
                                break;
                        }
                    }
                });

                string diretorioImportados = Path.Combine(DiretorioImportacaoIn, "Importados");
                string nomeArquivoSaida = Guid.NewGuid().ToString() + ".txt";

                GerarRelatorio(nomeArquivoSaida, vendedores, clientes, vendas);

                MoverArquivo(e.FullPath, nomeArquivoSaida, diretorioImportados);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ocorreu um erro com o arquivo {0}", e.Name);
                Console.WriteLine("Erro: {0}", ex.Message);
            }
        }

        private static void DadosVendedor(string[] data, ConcurrentBag<Vendedor> vendedores)
        {
            if (data.Length == 4)
            {
                vendedores.Add(new Vendedor()
                {
                    CPF = data[1],
                    Name = data[2],
                    Salary = decimal.Parse(data[3].Replace(".", ","))
                });
            }
        }

        private static void DadosCliente(string[] data, ConcurrentBag<Cliente> clientes)
        {
            if (data.Length == 4)
            {
                clientes.Add(new Cliente()
                {
                    CNPJ = data[1],
                    Name = data[2],
                    BusinessArea = data[3]
                });
            }
        }

        private static void DadosVenda(string[] data, ConcurrentBag<Venda> vendas)
        {
            if (data.Length == 4)
            {
                Venda venda = new Venda()
                {
                    ID = int.Parse(data[1]),
                    SalesmanName = data[3]
                };

                var itensVenda = data[2].Replace("[", "").Replace("]", "").Split(',');

                if (itensVenda != null && itensVenda.Length > 0)
                {
                    foreach (var itemVenda in itensVenda)
                    {
                        var item = itemVenda.Split('-');

                        if (item != null && item.Length > 0)
                        {
                            venda.Items.Add(new ItemVenda()
                            {
                                ID = int.Parse(item[0]),
                                Quantity = int.Parse(item[1]),
                                Price = decimal.Parse(item[2].Replace(".", ","))
                            });
                        }
                    }
                }

                vendas.Add(venda);
            }
        }

        private static void GerarRelatorio(string nomeArquivo, ConcurrentBag<Vendedor> vendedores, ConcurrentBag<Cliente> clientes, ConcurrentBag<Venda> vendas)
        {
            string arquivoSaida = Path.Combine(DiretorioImportacaoOut, nomeArquivo);

            using (StreamWriter sw = File.CreateText(arquivoSaida))
            {
                sw.WriteLine("Quantidade de clientes no arquivo de entrada: {0}", clientes.GroupBy(x => x.CNPJ).Count());
                sw.WriteLine("Quantidade de vendedores no arquivo de entrada: {0}", vendedores.GroupBy(x => x.CPF).Count());
                sw.WriteLine("ID da venda mais cara: {0}", vendas.OrderByDescending(x => x.Items.Sum(k => k.Price)).FirstOrDefault().ID);
                sw.WriteLine("O pior vendedor: {0}", vendas.GroupBy(x => x.SalesmanName).Select(x => x.OrderBy(k => k.Items.Sum(y => y.Price))).First().First().SalesmanName);
            }
        }

        private static void MoverArquivo(string arquivoAtual, string nomeArquivo, string diretorioDestino)
        {
            if (!Directory.Exists(diretorioDestino))
            {
                Directory.CreateDirectory(diretorioDestino);
            }

            var caminhoDestinoArquivo = Path.Combine(diretorioDestino, nomeArquivo);

            File.Move(arquivoAtual, caminhoDestinoArquivo);
        }

        private static bool IsFileLocked(string path)
        {
            FileStream stream = null;
            
            try
            {
                stream = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }
    }
}
