﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnaliseDadosVendas.Model
{
    public class Venda
    {
        public Venda()
        {
            Items = new List<ItemVenda>();
        }

        public int ID { get; set; }
        public string SalesmanName { get; set; }

        public List<ItemVenda> Items { get; set; }
    }
}
