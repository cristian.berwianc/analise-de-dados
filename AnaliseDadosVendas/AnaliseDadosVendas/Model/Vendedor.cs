﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnaliseDadosVendas.Model
{
    public class Vendedor
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
    }
}
